import {Component, OnInit, OnDestroy} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ChartData } from './chart-data';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pie-charts';
  chartDataMap: Map<number, ChartData>;
  public pieChartType:string = 'pie';
  data: string;
  keys = [];

  constructor(public http: HttpClient) {

  }
    ngOnInit() {
     this.chartDataMap = new Map<number, ChartData>();
      this.http.get(environment.csvUrl).subscribe(content => {
        this.data = content.toString();
        this.processData();
      });

    }

    private processData() {
      let rows = this.data.split("\n");
                const fLine = rows[0];
                let headers = fLine.split(",");
                let headerNameToIndex = new Map<string, number>();
                headers.forEach(h => headerNameToIndex[headers.indexOf(h)] = h);
                let headerToContentCount = new Map<number, Map<string, number>>();
                rows.splice(0, 1);
                rows.forEach(line => {
                    const colData = line.split(",");
                    Object.keys(headerNameToIndex).forEach(k => {
                        //cell data content
                        const cellContent = colData[k];
                        let contentToCountMap = headerToContentCount[k];
                        if(contentToCountMap === undefined) {
                            let contentToCount = new Map<string, number>();
                            contentToCountMap = new Map<number, Map<string, number>>();
                            contentToCountMap[k] = contentToCount;
                        }
                        // index to word-count map
                        let map = contentToCountMap[k];
                        if(map === undefined) {
                            map = new Map<string, number>();
                            map[cellContent] = -1;
                        }
                        let cnt = map[cellContent];
                        if(cnt < 0 || cnt === undefined) {
                            cnt = 0;
                        }
                        map[cellContent] = cnt + 1;
                        contentToCountMap[k] = map;
                        headerToContentCount[k] = contentToCountMap;
                    });
                });

                Object.keys(headerToContentCount).forEach(ind => {
                              let mapOfData = headerToContentCount[ind];
                              let labels = [];
                              let lValues = [];
                              Object.keys(mapOfData).forEach(k => {
                                  const map = mapOfData[k];
                                  Object.keys(map).forEach(lbl => {
                                      labels.push(lbl);
                                  });

                                  Object.keys(map).forEach(mv => {
                                     lValues.push(map[mv]);
                                  });
                              });
                              let cdata: ChartData = {
                                  pieChartLabels: labels,
                                  pieChartData: lValues,
                                  name: headerNameToIndex[ind]
                              };
                              this.chartDataMap[ind] = cdata;
                              this.keys =  Object.keys(this.chartDataMap);
                              console.log(this.chartDataMap);
                });
    }
    // events
    public chartClicked(e:any):void {
      console.log(e);
    }

    public chartHovered(e:any):void {
      console.log(e);
    }
}
