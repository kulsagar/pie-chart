export class ChartData {
    pieChartLabels: string[];
    pieChartData: number[];
    name: string;
}
